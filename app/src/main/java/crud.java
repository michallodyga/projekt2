package barber.barberapp;

import android.content.Context;

public interface crud {
    public boolean add( String data[], Context ctx );
    public boolean delete( Integer id, Context ctx );
    public String[] getAll( Context ctx );
    public boolean clearAll( Context ctx );
}
