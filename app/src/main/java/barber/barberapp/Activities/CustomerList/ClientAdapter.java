package barber.barberapp.Activities.CustomerList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import barber.barberapp.Model.Customer;
import barber.barberapp.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by micha on 10.05.2016.
 */

public class ClientAdapter extends ArrayAdapter implements Filterable {
    private List<Customer> clientList = null;
    private ItemFilter mFilter = new ItemFilter();

    public ClientAdapter(Context context, int resource) {
        super(context, resource);
        clientList = Customer.getAll(context);
    }

    private static class DataHandler {
        TextView name;
        TextView nr;
    }

    @Override
    public int getCount() {
        return this.clientList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.clientList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        DataHandler handler;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.customer_list_row, parent, false);
            handler = new DataHandler();
            handler.name = (TextView) row.findViewById(R.id.name);
            handler.nr = (TextView) row.findViewById(R.id.nr);
            row.setTag(handler);
        } else {
            handler = (DataHandler) row.getTag();
        }

        Customer customer;
        customer = (Customer) this.getItem(position);
        String fullName = customer.getFieldName()[0] + " " + customer.getFieldName()[1];
        handler.name.setText(fullName);
        handler.nr.setText(customer.getFieldName()[3] + "\n" + customer.getFieldName()[2]);

        return row;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            boolean found;
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final List<Customer> list = clientList;
            int count = list.size();
            final List<Customer> nlist = new ArrayList<>(count);
            Customer filterableCustomer;

            for (int i = 0; i < count; i++) {
                found = false;
                filterableCustomer = list.get(i);
                for (int j = 0; j < filterableCustomer.getFieldName().length; j++) {
                    if (filterableCustomer.getFieldName()[j].toLowerCase().contains(filterString)) {
                        found = true;
                    }
                }
                if(found)
                    nlist.add(filterableCustomer);
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clientList = (List<Customer>) results.values;
            notifyDataSetChanged();
        }
    }
}