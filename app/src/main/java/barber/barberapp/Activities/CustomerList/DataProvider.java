package barber.barberapp.Activities.CustomerList;

public class DataProvider {
    private String name;
    private String nr;

    public DataProvider(String name, String nr){
        this.setName(name);
        this.setNr(nr);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNr() {
        return nr;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }
}
