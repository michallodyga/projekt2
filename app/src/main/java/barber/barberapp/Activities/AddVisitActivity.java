package barber.barberapp.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import barber.barberapp.Model.Visit;
import barber.barberapp.R;

import java.util.Calendar;

public class AddVisitActivity extends Sidebar {

    private EditText customerIDField;
    private EditText visitDateField;
    private TimePicker visitTimeField;
    private TextView resultTxt;
    final Calendar cal = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_add_visit, null, false);
        drawer.addView(contentView, 0);

        customerIDField = (EditText) findViewById(R.id.customerID);
        visitDateField = (EditText) findViewById(R.id.dateOfVisit);
        visitTimeField = (TimePicker) findViewById(R.id.timeOfVisit);
        resultTxt = (TextView) findViewById(R.id.resultText);

        visitTimeField.setIs24HourView(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Pola zostały wyczyszczone", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                //currentHour = cal.get(Calendar.HOUR_OF_DAY);
                //currentMinute = cal.get(Calendar.MINUTE);
                customerIDField.setText("");
                visitDateField.setText("");
                visitTimeField.setCurrentMinute(0);
                visitTimeField.setCurrentHour(12);
                resultTxt.setText("");
            }
        });
    }

    public void addVisit(View view) {
        String data[] = {
                customerIDField.getText().toString().trim(),
                visitDateField.getText().toString().trim(),
                Integer.toString(visitTimeField.getCurrentHour()) + "." + Integer.toString(visitTimeField.getCurrentMinute()),
        };

        try {
            Visit visit = new Visit(data);
            if (visit.add(visit, this.getApplicationContext())) {
                resultTxt.setText("Dodano wizytę");
            } else
                resultTxt.setText("Błąd podczas dodawania wizyty");
        } catch (Exception e) {
            System.out.println(e);
        }
        resultTxt.setText("Dodano wizytę klienta: " + customerIDField.getText().toString());
    }

    /* TODO: (optional) Add some small button near customer ID TextField redirecting to Customer List Activity for the ease of choice of customer ID */
}
