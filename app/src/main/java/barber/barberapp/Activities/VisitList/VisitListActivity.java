package barber.barberapp.Activities.VisitList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.widget.ListView;
import android.widget.TextView;
import barber.barberapp.Activities.CustomerList.CustomerListActivity;
import barber.barberapp.Activities.Sidebar;
import barber.barberapp.Model.Customer;
import barber.barberapp.Model.Visit;
import barber.barberapp.R;
import java.util.List;
import android.view.View;

public class VisitListActivity extends Sidebar {
    List<Visit> list = null;

    private ListView visitList;
    private TextView resultTxt;
    private SearchView searchView;
    private VisitAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.visit_list, null, false);
        drawer.addView(contentView, 0);
//        setContentView(R.layout.visit_list);
        visitList = (ListView)findViewById(R.id.listView);
        resultTxt = (TextView) findViewById(R.id.resultText);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adapter = new VisitAdapter( getApplicationContext(), R.layout.visit_list_row );
        visitList.setAdapter(adapter);

        searchView = (SearchView)findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//              if (searchView.isExpanded() && TextUtils.isEmpty(newText)) {
                callSearch(newText);
//              }
                return true;
            }

            public void callSearch(String query) {
                adapter.getFilter().filter(query);
            }

        });
    }

    public void deleteVisits( View view ) {
        AlertDialog.Builder builder = new AlertDialog.Builder( VisitListActivity.this );
        builder.setPositiveButton("Tak", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Visit.clearAll( getApplicationContext() );
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Nie", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        builder.setMessage("Czy na pewno chcesz usunąć listę wizyt?");
        builder.setTitle("Uwaga!");

        AlertDialog d = builder.create();
        d.show();
    }
}
