package barber.barberapp.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import barber.barberapp.Activities.CustomerList.CustomerListActivity;
import barber.barberapp.Activities.VisitList.VisitListActivity;
import barber.barberapp.R;
import barber.barberapp.SignIn;

public class MenuActivity extends AppCompatActivity {
    private static boolean isSignedIn = false;
    private TextView txt;
    private Button logButton;
    private Button addCusButton;
    private Button addVisButton;
    private Button cusListButton;
    private Button visListButton;

    public static void setIsSignedIn(boolean newValue){
        isSignedIn = newValue;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Intent cl = new Intent(getBaseContext(), Sidebar.class);
        startActivity(cl);

        logButton = (Button)findViewById(R.id.logButton);
        addCusButton = (Button)findViewById(R.id.addCustomerButton);
        addVisButton = (Button)findViewById(R.id.addVisitButton);
        cusListButton = (Button)findViewById(R.id.customerListButton);
        visListButton = (Button)findViewById(R.id.visitListButton);
        txt = (TextView)findViewById(R.id.resultText);

        Init();
        handleFloatingButton();
    }

    private void Init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Bundle extra = getIntent().getExtras();
        String role = extra.getString("authorizationLvl");
        String userId = extra.getString("userId");

        setSupportActionBar(toolbar);
        txt.setText("Twoje id to:" + userId);
/*
        if (!Facade.userIsAdmin(this.getApplicationContext()) ) {
            cusListButton.setEnabled(false);
            visListButton.setEnabled(false);
        }

        if(isSignedIn) {
            logButton.setText("Wyloguj");
        } else{
            logButton.setText("Zaloguj");
        }
        */
    }

    private void handleFloatingButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void addCustomer(View view){
        Intent addCustomerIntent = new Intent(getBaseContext(), AddCustomerActivity.class);
        // addCustomerIntent.putExtra("loginEmail", emailInput);
        startActivity(addCustomerIntent);
    }

    public void addVisit(View view){
        Intent addVisitIntent = new Intent(getBaseContext(), AddVisitActivity.class);
        // For opening websites
        // Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
        startActivity(addVisitIntent);
    }

    public void moveToCustomerList(View view){
        Intent cl = new Intent(getBaseContext(), CustomerListActivity.class);
        startActivity(cl);
    }

    public void moveToVisitList(View view){
        Intent vl = new Intent(getBaseContext(), VisitListActivity.class);
        startActivity(vl);
    }

    public void LogInOrLogOut(View view){
        if(isSignedIn){
            AlertDialog.Builder builder = new AlertDialog.Builder( MenuActivity.this );
            builder.setPositiveButton("Tak", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    logButton.setText("Zaloguj");
                    isSignedIn = false;
                    addVisButton.setEnabled(false);
                    addCusButton.setEnabled(false);
                    visListButton.setEnabled(false);
                    cusListButton.setEnabled(false);
                }
            });
            builder.setNegativeButton("Nie", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });

            builder.setMessage("Czy na pewno chcesz się wylogować?");
            builder.setTitle("Uwaga!");

            AlertDialog d = builder.create();
            d.show();
        } else {
            Intent signInIntent = new Intent(getBaseContext(), SignIn.class);
            startActivity(signInIntent);
        }

    }


}
