package barber.barberapp;


import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class DataStorage /*implements barber.barberapp.crud*/ {
    private String[] fieldName = {"id", "imie", "nazwisko", "email", "tel"};

    private class AppendableObjectOutputStream extends ObjectOutputStream {
        public AppendableObjectOutputStream(OutputStream out) throws IOException {
            super(out);
        }
        //"id":1,"imie":"anna","nazwisko":"sowa","adnotacje":"","mail":""
        //string klient = "id:1 imie:a nazwisko:b

        @Override
        protected void writeStreamHeader() throws IOException {
        }
    }
    //@Override
    public boolean add( String data[], File dir ) {
        Map client = new HashMap<String, Serializable>();
        for (int i = 0; i < 5; i++) {
            client.put( fieldName[i], data[i] );
        }

        FileOutputStream fileOut;
        ObjectOutputStream out;
        try {
            File file_new = new File( dir + "listaKlientow" );
            if( file_new.createNewFile() ) {
                fileOut = new FileOutputStream(file_new);
                out = new ObjectOutputStream(fileOut);
            } else {
                fileOut = new FileOutputStream(file_new, true);
                out = new AppendableObjectOutputStream(fileOut);
            }
            out.writeObject(client);
            out.close();
            fileOut.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return true;
    }

    //@Override
    public boolean delete(Integer id) {
        return false;
    }

    //@Override
    public String[] getAll(File dir) {
        try {
            LineNumberReader lnr = new LineNumberReader(new FileReader(new File(dir + "listaKlientow")));
            lnr.skip(Long.MAX_VALUE);
            int n = lnr.getLineNumber() + 1;
            String[] data = new String[n];

            FileInputStream fis = new FileInputStream(dir + "listaKlientow");
            ObjectInputStream ois = new ObjectInputStream(fis);
            for (int i = 0; i < n; i++) {
                try {
                    data[i] = ois.readObject().toString();
                } catch( ClassNotFoundException e3 ) {
                    e3.printStackTrace();
                }
                System.out.println(data[i]);
            }
            ois.close();
            return data;
        } catch( IOException e2 ) {
            e2.printStackTrace();
        }
        return null;
    }

}
