package barber.barberapp.Model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.*;

/**
 * Created by Dawid on 2016-05-09.
 */

public class Visit {
    public String[] fieldName = new String[3];

    public Visit( String data[] ) {
        for(int i = 0; i < fieldName.length; i++) {
            fieldName[i] = data[i];
        }
    }

    public boolean add(Visit visit, Context ctx) {
        String temp = "";
        int i;
        for (i = 0; i < fieldName.length - 1; i++) {
            temp += visit.fieldName[i] + ":";
        }
        temp += visit.fieldName[i];
        SharedPreferences sp = ctx.getSharedPreferences("listaWizyt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(newId(ctx), temp);
        editor.apply();

        return true;
    }

    private String newId(Context ctx) {
        ArrayList<Integer> temp = new ArrayList<>();
        SharedPreferences sp = ctx.getSharedPreferences("listaWizyt", Context.MODE_PRIVATE);
        Integer id;
        Map<String, ?> keys = sp.getAll();
        if (keys.size() == 0) {
            id = 1;
        } else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                temp.add(Integer.valueOf(entry.getKey()));
            }
            id = Collections.max(temp) + 1;
        }
        return id.toString();
    }

    public boolean delete(Integer id, Context ctx) {
        SharedPreferences sp = ctx.getSharedPreferences("listaWizyt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(id.toString());
        editor.commit();
        return true;
    }

    public static List<Visit> getAll(Context ctx) {
        ArrayList<Visit> visitList = new ArrayList<>();
        SharedPreferences sp = ctx.getSharedPreferences("listaWizyt", Context.MODE_PRIVATE);
        Map<String,?> keys = sp.getAll();
        String[][] tmp = new String[keys.size()][5];
        String[] temp;
        int i = 0;
        //clearAll(ctx);
        for(Map.Entry<String,?> entry : keys.entrySet()) {
            temp = entry.getValue().toString().split( "\\:", 3 );
            for(int j = 0; j < 3; j++)
                tmp[i][j] = temp[j];
            tmp[i][3] = entry.getKey();
            i++;
        }
        if( keys.size() > 1 ) {
            Arrays.sort(tmp, new Comparator<String[]>() {
                public int compare(String[] s1, String[] s2) {
                    return s2[3].compareTo(s1[3]);
                }
            });
        }


        for(int j = 0; j < keys.size(); j++)
            visitList.add( new Visit( tmp[j] ) );

        return visitList;
    }

    public static boolean clearAll(Context ctx) {
        SharedPreferences sp = ctx.getSharedPreferences("listaWizyt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
        return true;
    }
}