package barber.barberapp.Model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.*;

/**
 * Created by Kuba on 2016-05-09.
 */

public class Customer {
    private String[] fieldName = new String[4];

    public Customer( String data[] ) {
        for(int i = 0; i < fieldName.length; i++) {
            fieldName[i] = data[i];
        }
    }

    public String[] getFieldName() {
        return fieldName;
    }

    public boolean add( Customer client, Context ctx ) {
        String temp = "";
        int i;
        for(i = 0; i < fieldName.length-1; i++) {
            temp += client.fieldName[i] + ":";
        }
        temp += client.fieldName[i];
        SharedPreferences sp = ctx.getSharedPreferences("listaKlientow", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString( newId(ctx), temp);
        editor.apply();

        return true;
    }

    private String newId( Context ctx ) {
        ArrayList<Integer> temp = new ArrayList<>();
        SharedPreferences sp = ctx.getSharedPreferences("listaKlientow", Context.MODE_PRIVATE);
        Integer id;
        Map<String,?> keys = sp.getAll();
        if( keys.size() == 0 ) {
            id = 1;
        } else {
            for (Map.Entry<String, ?> entry : keys.entrySet()) {
                temp.add(Integer.valueOf(entry.getKey()));
            }
            id = Collections.max(temp) + 1;
        }

        return id.toString();
    }

    public boolean delete(Integer id, Context ctx ) {
        SharedPreferences sp = ctx.getSharedPreferences( "listaKlientow", Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sp.edit();
        editor.remove( id.toString() );
        editor.commit();
        return true;
    }

    public static List<Customer> getAll( Context ctx ) {
        ArrayList<Customer> clientList = new ArrayList<>();
        SharedPreferences sp = ctx.getSharedPreferences("listaKlientow", Context.MODE_PRIVATE);
        Map<String,?> keys = sp.getAll();
        String[][] tmp = new String[keys.size()][5];
        String[] temp;
        int i = 0;
        for(Map.Entry<String,?> entry : keys.entrySet()) {
            temp = entry.getValue().toString().split( "\\:", 4 );
            for(int j = 0; j < 4; j++)
                tmp[i][j] = temp[j];
            tmp[i][4] = entry.getKey();
            i++;
        }
        if( keys.size() > 1 ) {
            Arrays.sort(tmp, new Comparator<String[]>() {
                public int compare(String[] s1, String[] s2) {
                    return s2[4].compareTo(s1[4]);
                }
            });
        }

        for(int j = 0; j < keys.size(); j++)
            clientList.add( new Customer( tmp[j] ) );

        return clientList;
    }

    public static boolean clearAll( Context ctx ){
        SharedPreferences sp = ctx.getSharedPreferences("listaKlientow", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
        return true;
    }
}