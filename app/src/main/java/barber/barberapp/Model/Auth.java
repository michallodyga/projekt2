package barber.barberapp.Model;

import android.content.Context;
import android.content.SharedPreferences;

public class Auth {
    private static String id;
    public static boolean userLoggedIn(Context ctx) {
        SharedPreferences s = ctx.getSharedPreferences("auth", Context.MODE_PRIVATE);
        String auth = s.getString("auth", "null");

        return !auth.equals("null");
    }

    public static boolean login(String name, String pass, Context ctx) {
        id = User.userExist(name, pass);

        if (id.equals("0")) {
            return false;
        } else {
            save(ctx);
            return true;
        }
    }

    public static boolean logout(Context ctx) {
        SharedPreferences s = ctx.getSharedPreferences("auth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = s.edit();
        editor.clear();
        editor.commit();
        return true;
    }

    private static void save(Context ctx) {
        SharedPreferences s = ctx.getSharedPreferences("auth", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = s.edit();
        editor.putString("auth", id);
        editor.apply();
    }

    public static boolean userIsAdmin(Context ctx) {

        SharedPreferences s = ctx.getSharedPreferences("auth", Context.MODE_PRIVATE);
        String id = s.getString("auth", "");
        return User.checkPrivileges(id).equals("0");
    }
}
